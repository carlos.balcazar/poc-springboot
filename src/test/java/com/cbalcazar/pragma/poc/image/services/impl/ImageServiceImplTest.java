package com.cbalcazar.pragma.poc.image.services.impl;

import com.cbalcazar.pragma.poc.exeptions.BusinnessImageException;
import com.cbalcazar.pragma.poc.image.repository.IImageMongoRepo;
import com.cbalcazar.pragma.poc.persistence.modelMongo.Image;
import com.cbalcazar.pragma.poc.person.dto.ImageDto;
import com.cbalcazar.pragma.poc.util.DataImageTest;
import com.cbalcazar.pragma.poc.util.SequenceGeneratorService;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

@SpringBootTest
class ImageServiceImplTest {

    @Mock
    ModelMapper modelMapper;

    @Mock
    IImageMongoRepo iImageMongoRepo;

    @Mock
    SequenceGeneratorService sequenceGeneratorService;

    @InjectMocks
    ImageServiceImpl imageService;

    @Test
    void testGetAll() {
        // Given
        List<Image> images = new ArrayList<>();

        images.add(DataImageTest.IMAGE);
        images.add(DataImageTest.IMAGE2);
        images.add(DataImageTest.IMAGE3);

        // When
        when(iImageMongoRepo.findAll()).thenReturn(images);
        when(modelMapper.map(any(), eq(ImageDto.class))).thenReturn(DataImageTest.IMAGE_DTO);

        List<ImageDto> imageDtos = imageService.getAll();

        // Then
        assertAll(
                () -> assertFalse(imageDtos.isEmpty()),
                () -> assertEquals(3, imageDtos.size()),
                () -> verify(iImageMongoRepo).findAll()
        );
    }

    @Test
    void testGetById() {
        // Given
        String expected = "c2FkYXM=";

        Image image = DataImageTest.IMAGE;
        image.setImage(new Binary(BsonBinarySubType.BINARY, "sadas".getBytes()));

        // When
        when(iImageMongoRepo.findById(anyLong())).thenReturn(Optional.of(image));

        String actual = imageService.getById(1L);

        // Then
        verify(iImageMongoRepo).findById(anyLong());
        assertEquals(expected, actual);

    }

    @Test
    void testSave() throws IOException {
        // Given
        MockMultipartFile file = new MockMultipartFile("file", "orig", null, "bar".getBytes());
        // When
        when(sequenceGeneratorService.generateSequence(anyString())).thenReturn(4L);
        Long imageId = imageService.save(file);

        // Then
        assertAll(
                () -> verify(sequenceGeneratorService).generateSequence(anyString()),
                () -> verify(iImageMongoRepo).save(any(Image.class)),
                () -> assertEquals(4L, imageId)
        );
    }

    @Test
    void testUpdateImageExeption() {
        // Given

        // When
        when(iImageMongoRepo.findById(1L)).thenThrow(BusinnessImageException.class);

        // Then
        assertThrows(BusinnessImageException.class, () -> {
            imageService.update(anyLong(), null);
        });
    }

    @Test
    void testUpdate() throws IOException {
        // Given
        MockMultipartFile file = new MockMultipartFile("file", "orig", null, "bar".getBytes());
        // When
        when(iImageMongoRepo.findById(1L)).thenReturn(Optional.of(DataImageTest.IMAGE));
        imageService.update(1L, file);
        // Then
        verify(iImageMongoRepo).findById(anyLong());
        verify(iImageMongoRepo).save(any(Image.class));
    }

    @Test
    void testDeleteImageExeption() {
        // Given

        // When
        when(iImageMongoRepo.findById(1L)).thenThrow(BusinnessImageException.class);

        // Then
        assertThrows(BusinnessImageException.class, () -> {
            imageService.delete(anyLong());
        });
    }

    @Test
    void testDelete() {
        // Given
        // When
        when(iImageMongoRepo.findById(1L)).thenReturn(Optional.of(DataImageTest.IMAGE));
        imageService.delete(1L);
        // Then
        assertAll(
                () -> verify(iImageMongoRepo).findById(anyLong()),
                () -> verify(iImageMongoRepo).deleteById(anyLong())
        );
    }
}
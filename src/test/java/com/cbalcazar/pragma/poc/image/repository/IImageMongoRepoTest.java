package com.cbalcazar.pragma.poc.image.repository;

import com.cbalcazar.pragma.poc.persistence.modelMongo.Image;
import com.cbalcazar.pragma.poc.util.DataImageTest;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataMongoTest
@ExtendWith(SpringExtension.class)
class IImageMongoRepoTest {

    @Autowired
    private IImageMongoRepo imageMongoRepo;

    @BeforeEach
    void setUp() {
        imageMongoRepo.save(DataImageTest.IMAGE);
        imageMongoRepo.save(DataImageTest.IMAGE2);
        imageMongoRepo.save(DataImageTest.IMAGE3);
    }

    @AfterEach
    void tearDown() {
        imageMongoRepo.deleteAll();
    }

    @Test
    void testFindAll() {
        // Given

        // When
        List<Image> imageList = imageMongoRepo.findAll();

        // Then
        assertAll(
                () -> assertFalse(imageList.isEmpty()),
                () -> assertEquals(3, imageList.size())
        );
    }

    @Test
    void testFindById() {
        // Given

        // When
        Optional<Image> image = imageMongoRepo.findById(1L);

        // Then
        assertAll(
                () -> assertTrue(image.isPresent()),
                () -> assertEquals(1L, image.orElseThrow().getId())
        );
    }

    @Test
    void testFindByIdThrowExecption() {
        // Given
        // When
        Optional<Image> image = imageMongoRepo.findById(200L);

        // Then
        assertThrows(NoSuchElementException.class, image::orElseThrow);
    }

    @Test
    void testUpdate() {
        // Given
        Image image = imageMongoRepo.findById(1L).orElseThrow();

        image.setDescription("Image1Update");
        image.setImage(new Binary(BsonBinarySubType.BINARY, "Image1Update".getBytes()));

        // When
        Image newImage = imageMongoRepo.save(image);

        // Then
        assertEquals("Image1Update", newImage.getDescription());
    }

    @Test
    void testDelete() {
        // Given
        Image image = imageMongoRepo.findById(1L).orElseThrow();

        // When
        imageMongoRepo.delete(image);

        // Then
        assertThrows(NoSuchElementException.class, () -> {
            imageMongoRepo.findById(1L).orElseThrow();
        });

    }
}
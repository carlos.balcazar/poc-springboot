package com.cbalcazar.pragma.poc.image.controller;

import com.cbalcazar.pragma.poc.image.services.IImageService;
import com.cbalcazar.pragma.poc.person.dto.ImageDto;
import com.cbalcazar.pragma.poc.util.DataImageTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(ImageController.class)
class ImageControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IImageService iImageService;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void testAllImages() throws Exception {
        // Given
        List<ImageDto> imageDtos = Arrays.asList(
                DataImageTest.IMAGE_DTO,
                DataImageTest.IMAGE_DTO,
                DataImageTest.IMAGE_DTO
        );
        // When
        when(iImageService.getAll()).thenReturn(imageDtos);

        mockMvc.perform(get("/api/image/").contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(imageDtos)))
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(3)))
                .andExpect(jsonPath("$[0].id").value(1L));

        verify(iImageService).getAll();
    }

    @Test
    void testOneImage() throws Exception {
        // Given

        // When
        when(iImageService.getById(1L)).thenReturn("UHJ1ZWJh");

        mockMvc.perform(get("/api/image/1").contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")))
                .andExpect(jsonPath("$").isString())
                .andExpect(jsonPath("$").value("UHJ1ZWJh"));
        // Then
        verify(iImageService).getById(1L);
    }

    @Test
    void testAddImage() throws Exception {
        // Given
        MockMultipartFile file = new MockMultipartFile("image", "orig", null, "bar".getBytes());

        // When
        when(iImageService.save(file)).thenReturn(1L);

        mockMvc.perform(multipart("/api/image/")
                        .file(file))
                // Then
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.image_id").isNumber())
                .andExpect(jsonPath("$.message").value("Added Successfully"));
        // Then
        verify(iImageService).save(file);
    }

    @Test
    void testUpdateImage() throws Exception {
        // Given
        MockMultipartFile file = new MockMultipartFile("image", "orig", null, "bar".getBytes());

        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/api/image/1");
        builder.with(request -> {
            request.setMethod("PUT");
            return request;
        });
        // When
        mockMvc.perform(builder
                        .file(file))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")))
                .andExpect(jsonPath("$").value("Update Successfully"));

        // Then
        verify(iImageService).update(1L, file);
    }

    @Test
    void testDeleteImage() throws Exception {
        // Given

        // When
        mockMvc.perform(delete("/api/image/1").contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")))
                .andExpect(jsonPath("$").isString())
                .andExpect(jsonPath("$").value("Delete Successfully"));

        // Then
        verify(iImageService).delete(1L);
    }
}
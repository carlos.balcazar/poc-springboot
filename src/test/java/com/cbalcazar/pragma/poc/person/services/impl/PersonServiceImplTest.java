package com.cbalcazar.pragma.poc.person.services.impl;

import com.cbalcazar.pragma.poc.exeptions.BusinnessPersonException;
import com.cbalcazar.pragma.poc.image.services.IImageService;
import com.cbalcazar.pragma.poc.persistence.models.Person;
import com.cbalcazar.pragma.poc.persistence.models.TypesDocument;
import com.cbalcazar.pragma.poc.person.dto.CityDto;
import com.cbalcazar.pragma.poc.person.dto.PersonDto;
import com.cbalcazar.pragma.poc.person.dto.TypesDocumentDto;
import com.cbalcazar.pragma.poc.person.repository.IPersonRepository;
import com.cbalcazar.pragma.poc.util.DataPersonTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.modelmapper.ModelMapper;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class PersonServiceImplTest {

    @Mock
    ModelMapper modelMapper;
    @Mock
    ObjectMapper objectMapper;
    @Mock
    IPersonRepository personRepository;
    @Mock
    IImageService iImageService;

    @InjectMocks
    PersonServiceImpl personService;

    @BeforeEach
    void setUp() {
    }

    @AfterEach
    void tearDown() {
    }

    @Test
    void testGetAll() {
        // Given
        List<Person> personList = new ArrayList<>();

        personList.add(DataPersonTest.PERSON);
        personList.add(DataPersonTest.PERSON2);
        personList.add(DataPersonTest.PERSON3);

        Page<Person> personPage = new PageImpl(personList, PageRequest.of(0, 10), personList.size());

        // When
        when(this.personRepository.findAll(isA(Pageable.class))).thenReturn(personPage);
        when(modelMapper.map(any(), eq(PersonDto.class))).thenReturn(DataPersonTest.PERSON_DTO);
        when(modelMapper.map(any(), eq(TypesDocumentDto.class))).thenReturn(DataPersonTest.PERSON_DTO.getTypesDocuments());
        when(modelMapper.map(any(), eq(CityDto.class))).thenReturn(DataPersonTest.PERSON_DTO.getCities());

        Page<PersonDto> personDtoPage = personService.getAll(
                0,
                10,
                true
        );

        // Then
        assertEquals(3, personDtoPage.getTotalElements());

        verify(personRepository).findAll(isA(Pageable.class));

    }

    @Test
    void testGetPersonById() {
        // Given
        Person person = DataPersonTest.createPerson1();
        PersonDto personDto = DataPersonTest.createPersonDto1();
        // When
        when(personRepository.findById(1L)).thenReturn(Optional.of(person));

        when(modelMapper.map(any(), eq(PersonDto.class))).thenReturn(personDto);
        when(modelMapper.map(any(), eq(TypesDocumentDto.class))).thenReturn(personDto.getTypesDocuments());
        when(modelMapper.map(any(), eq(CityDto.class))).thenReturn(personDto.getCities());

        PersonDto personDtoNew = personService.getPersonById(1L);
        // Then
        assertAll(
                () -> {
                    assertNotNull(personDtoNew);
                },
                () -> {
                    assertEquals(1L, personDtoNew.getId());
                }
        );

        verify(personRepository, times(2)).findById(1L);
    }

    @Test
    void testGetPersonByIdPersonExeption() {
        // Given

        // When
        when(personRepository.findById(anyLong())).thenThrow(BusinnessPersonException.class);

        // Then
        assertThrows(BusinnessPersonException.class, () -> {
            personService.getPersonById(anyLong());
        });
    }

    @Test
    void testGetPersonByTypeAndDocument() {
        // Given
        TypesDocument typesDocument = DataPersonTest.PERSON.getTypesDocuments();
        String document = "1151956458";
        // When
        when(personRepository.findTopByTypesDocumentsAndDocumentNumber(
                typesDocument,
                document
        )).thenReturn(Optional.of(DataPersonTest.PERSON));

        when(modelMapper.map(any(), eq(PersonDto.class))).thenReturn(DataPersonTest.PERSON_DTO);
        when(modelMapper.map(any(), eq(TypesDocumentDto.class))).thenReturn(DataPersonTest.PERSON_DTO.getTypesDocuments());
        when(modelMapper.map(any(), eq(CityDto.class))).thenReturn(DataPersonTest.PERSON_DTO.getCities());

        PersonDto personDto = personService.getPersonByTypeAndDocument(1L, document);

        // Then
        verify(personRepository).findTopByTypesDocumentsAndDocumentNumber(typesDocument, document);

        assertEquals(document, personDto.getDocumentNumber());

    }

    @Test
    void testGetPersonByTypeAndDocumentPersonExeption() {
        // Given

        // When
        when(personRepository.findTopByTypesDocumentsAndDocumentNumber(any(TypesDocument.class), anyString()))
                .thenThrow(BusinnessPersonException.class);

        assertThrows(BusinnessPersonException.class, () -> {
            personService.getPersonByTypeAndDocument(anyLong(), anyString());
        });
    }

    @Test
    void testGetOlderPersons() {
        // Given
        Integer years = 15;
        LocalDate date = LocalDate.now().minusYears(years);

        List<Person> personList = new ArrayList<>();

        personList.add(DataPersonTest.PERSON);
        personList.add(DataPersonTest.PERSON3);

        List<PersonDto> personDtoList = new ArrayList<>();

        personDtoList.add(DataPersonTest.PERSON_DTO);
        personDtoList.add(DataPersonTest.PERSON_DTO3);

        // When

        when(personRepository.findByBirthDateLessThanEqual(date)).thenReturn(personList);

        personDtoList.stream().forEach(personDto -> {
            PersonDto personDto1 = personDto;
            when(modelMapper.map(any(), eq(PersonDto.class))).thenReturn(personDto1);
            when(modelMapper.map(any(), eq(TypesDocumentDto.class))).thenReturn(personDto1.getTypesDocuments());
            when(modelMapper.map(any(), eq(CityDto.class))).thenReturn(personDto1.getCities());
        });


        List<PersonDto> personDtoListReal = personService.getOlderPersons(years);

        // Then
        assertAll(
                () -> assertEquals(2, personDtoListReal.size()),
                () -> assertFalse(personDtoListReal.isEmpty())
        );

        verify(personRepository).findByBirthDateLessThanEqual(date);
    }

    @Test
    void testInsertWithImage() throws IOException {
        // Given
        MultipartFile image = mock(MultipartFile.class);

        // When
        when(objectMapper.readValue(anyString(), eq(PersonDto.class))).thenReturn(DataPersonTest.PERSON_DTO);
        when(modelMapper.map(any(), eq(Person.class))).thenReturn(DataPersonTest.PERSON);

        when(iImageService.save(image)).thenReturn(anyLong());

        personService.insert(anyString(), image);

        // Then
        verify(personRepository).save(any(Person.class));
    }

    @Test
    void testInsertNullPointerException() throws IOException {
        // Given

        // When
        when(objectMapper.readValue(anyString(), eq(PersonDto.class))).thenThrow(NullPointerException.class);

        // Then
        assertThrows(NullPointerException.class, () -> {
            personService.insert(anyString(), eq(null));
        });

    }

    @Test
    void testInsertWithOutImage() throws IOException {
        // Given

        // When
        when(objectMapper.readValue(anyString(), eq(PersonDto.class))).thenReturn(DataPersonTest.PERSON_DTO);
        when(modelMapper.map(any(), eq(Person.class))).thenReturn(DataPersonTest.PERSON);

        personService.insert(anyString(), eq(null));

        // Then
        verify(personRepository).save(any(Person.class));
        verify(iImageService, never()).save(any(MultipartFile.class));
    }

    @Test
    void testUpdatePersonExeption() throws IOException {
        // Given

        // When
        when(objectMapper.readValue(anyString(), eq(PersonDto.class))).thenReturn(DataPersonTest.PERSON_DTO);
        when(personRepository.findById(anyLong())).thenThrow(BusinnessPersonException.class);

        // Then
        assertThrows(BusinnessPersonException.class, () -> {
            personService.update(anyString(), eq(null));
        });

    }

    @Test
    void testUpdateNullPointerException() throws IOException {
        // Given

        // When
        when(objectMapper.readValue(anyString(), eq(PersonDto.class))).thenThrow(NullPointerException.class);

        // Then
        assertThrows(NullPointerException.class, () -> {
            personService.update(eq(""), eq(null));
        });

    }

    @Test
    @DisplayName("Actualizar persona e imagen")
    void testUpdateWithImage() throws IOException {
        // Given
        MultipartFile image = mock(MultipartFile.class);
        PersonDto personDto = DataPersonTest.PERSON_DTO2;
        Person person = DataPersonTest.PERSON2;
        person.setImageId(2L);

        // When
        when(objectMapper.readValue(anyString(), eq(PersonDto.class))).thenReturn(personDto);
        when(personRepository.findById(2L)).thenReturn(Optional.ofNullable(person));
        when(modelMapper.map(any(), eq(Person.class))).thenReturn(person);


        Long idPerson = personService.update("", image);
        // Then

        assertAll(
                () -> verify(personRepository).findById(2L),
                () -> assertEquals(idPerson, personDto.getId()),
                () -> verify(iImageService).update(2L, image),
                () -> verify(personRepository).save(person)
        );
    }

    @Test
    @DisplayName("Actualizar persona e insertar imagen")
    void testUpdateWithImage2() throws IOException {
        // Given
        MultipartFile image = mock(MultipartFile.class);
        Person person = DataPersonTest.PERSON2;
        person.setImageId(null);

        // When
        when(objectMapper.readValue(anyString(), eq(PersonDto.class))).thenReturn(DataPersonTest.PERSON_DTO2);
        when(personRepository.findById(2L)).thenReturn(Optional.ofNullable(person));
        when(modelMapper.map(any(), eq(Person.class))).thenReturn(person);

        Long idPerson = personService.update("", image);
        // Then

        assertAll(
                () -> verify(personRepository).findById(2L),
                () -> assertEquals(idPerson, DataPersonTest.PERSON2.getId()),
                () -> verify(iImageService).save(image),
                () -> verify(personRepository).save(DataPersonTest.PERSON2)
        );
    }

    @Test
    void testDeletePersonExeption() {
        // Given

        // When
        when(personRepository.findById(anyLong())).thenThrow(BusinnessPersonException.class);

        // Then
        assertThrows(BusinnessPersonException.class, () -> {
            personService.delete(anyLong());
        });
    }

    @Test
    void testDelete() {
        // Given
        String expected = "Person with id 1, has been successfully deleted";

        // When
        when(personRepository.findById(anyLong())).thenReturn(Optional.ofNullable(DataPersonTest.PERSON));
        String respuesta = personService.delete(1L);
        // Then
        assertAll(
                () -> verify(personRepository).findById(anyLong()),
                () -> verify(personRepository).deleteById(anyLong()),
                () -> verify(iImageService).delete(anyLong()),
                () -> assertEquals(expected, respuesta)
        );
    }
}
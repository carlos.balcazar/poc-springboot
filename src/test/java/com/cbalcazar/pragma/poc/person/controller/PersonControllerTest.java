package com.cbalcazar.pragma.poc.person.controller;

import com.cbalcazar.pragma.poc.person.dto.PersonDto;
import com.cbalcazar.pragma.poc.person.services.IPersonService;
import com.cbalcazar.pragma.poc.util.DataPersonTest;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMultipartHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@WebMvcTest(PersonController.class)
class PersonControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private IPersonService iPersonService;

    private ObjectMapper objectMapper;

    @BeforeEach
    void setUp() {
        objectMapper = new ObjectMapper();
    }

    @Test
    void testAllPerson() throws Exception {
        // Given
        List<PersonDto> personDtoList = Arrays.asList(
                DataPersonTest.PERSON_DTO,
                DataPersonTest.PERSON_DTO2,
                DataPersonTest.PERSON_DTO3
        );

        Page<PersonDto> personList = new PageImpl(personDtoList, PageRequest.of(0, 10), personDtoList.size());
        // When
        when(iPersonService.getAll(0, 10, true)).thenReturn(personList);

        mockMvc.perform(get("/api/person/")
                        .contentType(MediaType.APPLICATION_JSON)
                        .param("page", "0")
                        .param("size", "10")
                        .param("enablePagination", "true")
                )
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.content").isArray())
                .andExpect(jsonPath("$.content", hasSize(3)));

        verify(iPersonService).getAll(0, 10, true);
    }

    @Test
    void testOnePerson() throws Exception {
        // Given

        // When
        when(iPersonService.getPersonById(3L)).thenReturn(DataPersonTest.PERSON_DTO3);

        mockMvc.perform(get("/api/person/{id}", 3).contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                //.andExpect(content().json(objectMapper.writeValueAsString(DataPersonTest.PERSON_DTO)))
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.first_name").value("Jaime"));
        // Then
        verify(iPersonService).getPersonById(3L);
    }

    @Test
    void testGetPersonByTipoAndDocument() throws Exception {
        // Given

        // When
        when(iPersonService.getPersonByTypeAndDocument(3L, "1151")).thenReturn(DataPersonTest.PERSON_DTO3);

        mockMvc.perform(get("/api/person/{type_document}/{document_number}", 3, "1151")
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                //.andExpect(content().json(objectMapper.writeValueAsString(DataPersonTest.PERSON_DTO)))
                .andExpect(jsonPath("$.id", is(3)))
                .andExpect(jsonPath("$.first_name").value("Jaime"));
        // Then
        verify(iPersonService).getPersonByTypeAndDocument(3L, "1151");
    }

    @Test
    void testGetOlderPersons() throws Exception {
        // Given
        List<PersonDto> personDtoList = Arrays.asList(
                DataPersonTest.PERSON_DTO,
                DataPersonTest.PERSON_DTO2
        );

        // When
        when(iPersonService.getOlderPersons(12)).thenReturn(personDtoList);

        mockMvc.perform(get("/api/person/older/{years}", 12)
                        .contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk())
                .andExpect(jsonPath("$").isArray())
                .andExpect(jsonPath("$", hasSize(2)));

        verify(iPersonService).getOlderPersons(12);
    }

    @Test
    void testAddPerson() throws Exception {
        // Given
        PersonDto personDto = DataPersonTest.PERSON_DTO;
        personDto.setId(null);
        personDto.setBirthDate(null);

        MockMultipartFile file = new MockMultipartFile("image", "orig", null, "bar".getBytes());

        // When
        when(iPersonService.insert(objectMapper.writeValueAsString(personDto), file)).thenReturn(4L);

        // when
        mockMvc.perform(multipart("/api/person/")
                        .file(file)
                        .param("person", objectMapper.writeValueAsString(personDto)))
                // Then
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isNumber())
                .andExpect(jsonPath("$").value(4));
        // Then
        verify(iPersonService).insert(any(), any());

    }

    @Test
    void testUpdatePerson() throws Exception {
        // Given
        PersonDto personDto = DataPersonTest.PERSON_DTO;
        personDto.setBirthDate(null);

        MockMultipartFile file = new MockMultipartFile("image", "orig", null, "bar".getBytes());

        // When
        when(iPersonService.update(objectMapper.writeValueAsString(personDto), file)).thenReturn(3L);

        // when
        MockMultipartHttpServletRequestBuilder builder =
                MockMvcRequestBuilders.multipart("/api/person/");
        builder.with(request -> {
            request.setMethod("PUT");
            return request;
        });

        mockMvc.perform(builder
                        .file(file)
                        .param("person", objectMapper.writeValueAsString(personDto)))
                // Then
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$").isNumber())
                .andExpect(jsonPath("$").value(3));

        // Then
        verify(iPersonService).update(any(), any());
    }

    @Test
    void testDeletePerson() throws Exception {
        // Given

        // When

        when(iPersonService.delete(1L)).thenReturn("Person with id 1, has been successfully deleted");

        mockMvc.perform(delete("/api/person/{id}", 1L).contentType(MediaType.APPLICATION_JSON))
                // Then
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.valueOf("text/plain;charset=UTF-8")))
                .andExpect(jsonPath("$").isString())
                .andExpect(jsonPath("$").value("Person with id 1, has been successfully deleted"));

        // Then
        verify(iPersonService).delete(1L);
    }
}
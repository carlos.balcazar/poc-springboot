package com.cbalcazar.pragma.poc.person.repository;

import com.cbalcazar.pragma.poc.persistence.models.City;
import com.cbalcazar.pragma.poc.persistence.models.Person;
import com.cbalcazar.pragma.poc.persistence.models.TypesDocument;
import com.cbalcazar.pragma.poc.util.DataPersonTest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.time.LocalDate;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
class IPersonRepositoryTest {

    @Autowired
    IPersonRepository personRepository;

    @Test
    void testFindAll() {
        // Given
        // When
        List<Person> personList = personRepository.findAll();

        // Then
        assertAll(
                () -> assertFalse(personList.isEmpty()),
                () -> assertEquals(3, personList.size())
        );
    }

    @Test
    void testFindById() {
        // Given
        // When
        Optional<Person> person = personRepository.findById(1L);

        // Then
        assertAll(
                () -> assertTrue(person.isPresent()),
                () -> assertEquals("Carlos", person.orElseThrow().getFirstName())
        );
    }

    @Test
    void testFindByIdThrowExecption() {
        // Given
        // When
        Optional<Person> person = personRepository.findById(100L);

        // Then
        assertThrows(NoSuchElementException.class, person::orElseThrow);
    }

    @Test
    void testFindTopByTypesDocumentsAndDocumentNumber() {
        // Given
        TypesDocument typesDocument = DataPersonTest.PERSON3.getTypesDocuments();
        // When
        Optional<Person> person = personRepository.findTopByTypesDocumentsAndDocumentNumber(typesDocument, "115981715");

        // Then
        assertAll(
                () -> assertTrue(person.isPresent()),
                () -> assertEquals("Rojas", person.get().getLastName())
        );
    }

    @Test
    void testFindTopByTypesDocumentsAndDocumentNumberNull() {
        // Given
        TypesDocument typesDocument = DataPersonTest.PERSON3.getTypesDocuments();

        // When
        Optional<Person> person = personRepository.findTopByTypesDocumentsAndDocumentNumber(typesDocument, "123123123123123123");

        // Then
        assertAll(
                () -> assertTrue(person.isEmpty())
        );
    }

    @Test
    void testFindByBirthDateLessThanEqualThirtyYears() {
        // Given
        Integer years = 30; //0 Personas
        LocalDate date = LocalDate.now().minusYears(years);
        // When
        List<Person> personList = personRepository.findByBirthDateLessThanEqual(date);

        // Then
        assertAll(
                () -> assertTrue(personList.isEmpty())
        );
    }

    @Test
    void testFindByBirthDateLessThanEqualTwelveYears() {
        // Given
        Integer years = 12; //2 Personas
        LocalDate date = LocalDate.now().minusYears(years);
        // When
        List<Person> personList = personRepository.findByBirthDateLessThanEqual(date);

        // Then
        assertAll(
                () -> assertFalse(personList.isEmpty()),
                () -> assertEquals(2, personList.size())
        );
    }

    @Test
    void testSave() {
        // Given
        TypesDocument typesDocument = TypesDocument.builder().id(2L).build();

        Person person4 = Person
                .builder()
                .id(null)
                .firstName("Felipe")
                .lastName("Martinez")
                .typesDocuments(typesDocument)
                .documentNumber("9996633")
                .birthDate(LocalDate.parse("2018-06-06"))
                .cities(City.builder().id(2L).build())
                .build();

        // When
        Person newPerson = personRepository.save(person4);

        // Then
        assertAll(
                () -> assertTrue(newPerson != null),
                () -> assertEquals("9996633", newPerson.getDocumentNumber())
        );

    }

    @Test
    void testUpdate() {
        // Given
        Person person = personRepository.findById(1L).orElseThrow();

        person.setFirstName("Alberto");
        person.setLastName("Amaya");
        person.setDocumentNumber("1151547812827");

        // When
        Person newPerson = personRepository.save(person);

        // Then
        assertAll(
                () -> assertEquals("Alberto", newPerson.getFirstName()),
                () -> assertEquals("Amaya", newPerson.getLastName()),
                () -> assertEquals("1151547812827", newPerson.getDocumentNumber())
        );

    }

    @Test
    void testDelete() {
        // Given
        Person person = personRepository.findById(1L).orElseThrow();

        // When
        personRepository.delete(person);

        // Then
        assertThrows(NoSuchElementException.class, () -> {
            personRepository.findById(1L).orElseThrow();
        });

    }
}

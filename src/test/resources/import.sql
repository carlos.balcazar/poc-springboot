INSERT INTO cities (name) VALUES ('Bogota');
INSERT INTO cities (name) VALUES ('Cali');
INSERT INTO types_documents (name) VALUES ('CC');
INSERT INTO types_documents (name) VALUES ('TI');
INSERT INTO persons (first_name, last_name, type_document_id, document_number, birth_date, city_id, image_id) VALUES ('Carlos', 'Balcazar', 1, '1151956458', '1994-12-08', 1, 1);
INSERT INTO persons (first_name, last_name, type_document_id, document_number, birth_date, city_id, image_id) VALUES ('Jimena', 'Camayo', 2, '123456789', '2010-05-02', 2, 2);
INSERT INTO persons (first_name, last_name, type_document_id, document_number, birth_date, city_id, image_id) VALUES ('Jaime', 'Rojas', 1, '115981715', '1999-01-25', 1, 3);

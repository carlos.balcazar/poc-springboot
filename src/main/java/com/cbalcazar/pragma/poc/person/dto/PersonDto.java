package com.cbalcazar.pragma.poc.person.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

import javax.validation.constraints.NotEmpty;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@EqualsAndHashCode
public class PersonDto implements Serializable {

    private Long id;
    @NotEmpty
    @JsonProperty("first_name")
    private String firstName;
    @JsonProperty("last_name")
    private String lastName;
    @JsonProperty("type_document")
    private TypesDocumentDto typesDocuments;
    @NotEmpty
    @JsonProperty("document_number")
    private String documentNumber;
    @JsonProperty("birth_date")
    private LocalDate birthDate;
    @JsonProperty("city")
    private CityDto cities;
    @JsonProperty("image_id")
    private Long imageId;
}

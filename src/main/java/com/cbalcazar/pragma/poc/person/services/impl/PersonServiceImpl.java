package com.cbalcazar.pragma.poc.person.services.impl;

import com.cbalcazar.pragma.poc.exeptions.BusinnessPersonException;
import com.cbalcazar.pragma.poc.image.services.IImageService;
import com.cbalcazar.pragma.poc.persistence.models.Person;
import com.cbalcazar.pragma.poc.persistence.models.TypesDocument;
import com.cbalcazar.pragma.poc.person.dto.CityDto;
import com.cbalcazar.pragma.poc.person.dto.PersonDto;
import com.cbalcazar.pragma.poc.person.dto.TypesDocumentDto;
import com.cbalcazar.pragma.poc.person.repository.IPersonRepository;
import com.cbalcazar.pragma.poc.person.services.IPersonService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
@Transactional
public class PersonServiceImpl implements IPersonService {

    private static final String PERSON_NOT_FOUND_MESSAGE = "Person with ID %d was not found";

    private final ModelMapper modelMapper;

    private final ObjectMapper objectMapper;

    private final IPersonRepository personRepository;

    private final IImageService iImageService;

    @Autowired
    public PersonServiceImpl(
            ModelMapper modelMapper,
            ObjectMapper objectMapper,
            IPersonRepository personRepository,
            IImageService iImageService
    ) {
        this.modelMapper = modelMapper;
        this.objectMapper = objectMapper;
        this.personRepository = personRepository;
        this.iImageService = iImageService;
    }

    @Override
    public Page<PersonDto> getAll(Integer page, Integer size, boolean enablePagination) {

        Page<Person> persons = personRepository.findAll(enablePagination ? PageRequest.of(page, size) : Pageable.unpaged());

        Page<PersonDto> personDtos = persons.map(this::convertToDto);

        if (personDtos.isEmpty()) {
            throw new BusinnessPersonException("No persons found");
        }

        return personDtos;
    }

    @Override
    public PersonDto getPersonById(Long id) {

        personRepository.findById(id)
                .orElseThrow(() -> new BusinnessPersonException(String.format(PERSON_NOT_FOUND_MESSAGE, id)));

        Optional<Person> optionalPerson = personRepository.findById(id);

        return convertToDto(optionalPerson.orElse(null));
    }

    @Override
    public PersonDto getPersonByTypeAndDocument(
            Long typeDocument,
            String documentNumber
    ) {

        TypesDocument typesDocument = new TypesDocument();
        typesDocument.setId(typeDocument);

        Person person = personRepository.findTopByTypesDocumentsAndDocumentNumber(
                typesDocument, documentNumber
        ).orElseThrow(() -> new BusinnessPersonException(String.format("Person with ID %s was not found", documentNumber)));


        return convertToDto(person);
    }

    @Override
    public List<PersonDto> getOlderPersons(Integer years) {

        LocalDate date = LocalDate.now().minusYears(years);

        System.out.printf("date:" + date);

        List<Person> persons = personRepository.findByBirthDateLessThanEqual(date);

        return persons.stream().map(this::convertToDto).collect(Collectors.toList());
    }

    @Override
    public Long insert(String jsonPerson, MultipartFile image) {

        PersonDto personDto = convertToJson(jsonPerson);

        personDto.setId(null);

        Person person;
        try {
            person = convertToEntity(personDto);

            if (image != null) {
                Long imageId = iImageService.save(image);

                person.setImageId(imageId);

            }

            personRepository.save(person);
        } catch (Exception e) {
            throw new BusinnessPersonException("No persons found");
        }

        return person.getId();
    }

    @Override
    public Long update(String jsonPerson, MultipartFile image) {

        PersonDto personDto = convertToJson(jsonPerson);

        Long personId = personDto.getId();

        personRepository.findById(personId)
                .orElseThrow(() -> new BusinnessPersonException(String.format(PERSON_NOT_FOUND_MESSAGE, personId)));

        Person person = null;
        try {
            person = convertToEntity(personDto);

            if (person.getImageId() != null) {
                iImageService.update(person.getImageId(), image);
            } else {
                Long imageId = iImageService.save(image);

                person.setImageId(imageId);
            }

            personRepository.save(person);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return person.getId();
    }

    @Override
    public String delete(Long id) {

        Person person = personRepository.findById(id)
                .orElseThrow(() -> new BusinnessPersonException(String.format(PERSON_NOT_FOUND_MESSAGE, id)));

        iImageService.delete(person.getImageId());

        personRepository.deleteById(id);

        return String.format("Person with id %d, has been successfully deleted", id);
    }

    public PersonDto convertToDto(Person person) {


        PersonDto personDto = modelMapper.map(person, PersonDto.class);

        personDto.setTypesDocuments(modelMapper.map(
                person.getTypesDocuments(),
                TypesDocumentDto.class
        ));

        personDto.setCities(modelMapper.map(person.getCities(), CityDto.class));

        return personDto;
    }

    private Person convertToEntity(PersonDto personDto) {
        return modelMapper.map(personDto, Person.class);
    }

    private PersonDto convertToJson(String jsonPerson) {

        PersonDto personDto = PersonDto.builder().build();

        try {
            personDto = objectMapper.readValue(jsonPerson, PersonDto.class);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        return personDto;

    }
}

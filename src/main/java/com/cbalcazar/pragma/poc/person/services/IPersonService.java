package com.cbalcazar.pragma.poc.person.services;

import com.cbalcazar.pragma.poc.person.dto.PersonDto;
import org.springframework.data.domain.Page;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

public interface IPersonService {

    Page<PersonDto> getAll(Integer page, Integer size, boolean enablePagination);

    PersonDto getPersonById(Long id);

    PersonDto getPersonByTypeAndDocument(Long typeDocument, String documentNumber);

    List<PersonDto> getOlderPersons(Integer years);

    Long insert(String jsonPerson, MultipartFile image);

    Long update(String jsonPerson, MultipartFile image);

    String delete(Long id);

}

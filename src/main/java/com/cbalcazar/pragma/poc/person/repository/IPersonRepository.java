package com.cbalcazar.pragma.poc.person.repository;

import com.cbalcazar.pragma.poc.persistence.models.Person;
import com.cbalcazar.pragma.poc.persistence.models.TypesDocument;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

public interface IPersonRepository extends JpaRepository<Person, Long> {

    Optional<Person> findTopByTypesDocumentsAndDocumentNumber(TypesDocument typesDocuments, String documentNumber);

    List<Person> findByBirthDateLessThanEqual(LocalDate date);

}
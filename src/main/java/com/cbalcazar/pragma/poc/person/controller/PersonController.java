package com.cbalcazar.pragma.poc.person.controller;

import com.cbalcazar.pragma.poc.person.dto.PersonDto;
import com.cbalcazar.pragma.poc.person.services.IPersonService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@RestController
@RequestMapping("/api/person")
public class PersonController {

    @Autowired
    IPersonService iPersonServices;

    @Operation(summary = "Get all person")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Persons found",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = PersonDto.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid id supplied",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Persons not found",
                    content = @Content)})
    @GetMapping("/")
    public ResponseEntity<Page<PersonDto>> allPerson(
            @RequestParam(name = "page", required = false, defaultValue = "0") Integer page,
            @RequestParam(name = "size", required = false, defaultValue = "10") Integer size,
            @RequestParam(name = "enablePagination", required = false, defaultValue = "false") Boolean enablePagination
    ) {
        return ResponseEntity.status(HttpStatus.OK).body(iPersonServices.getAll(page, size, enablePagination));
    }

    @Operation(summary = "Get a person by its id")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the person",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PersonDto.class))}),
            @ApiResponse(responseCode = "400", description = "The id is invalid",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Person not found",
                    content = @Content)})
    @GetMapping("/{id}")
    public ResponseEntity<PersonDto> onePerson(
            @Parameter(description = "id of person to be searched")
            @PathVariable("id") Long id) {
        return ResponseEntity.status(HttpStatus.OK).body(iPersonServices.getPersonById(id));
    }

    @Operation(summary = "Get a person by document type and document number")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found the person",
                    content = {@Content(mediaType = "application/json",
                            schema = @Schema(implementation = PersonDto.class))}),
            @ApiResponse(responseCode = "400", description = "Invalid data",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Person not found",
                    content = @Content)})
    @GetMapping("/{type_document}/{document_number}")
    public ResponseEntity<PersonDto> getPersonByTipoAndDocument(
            @PathVariable("type_document") Long typeDocument,
            @PathVariable("document_number") String documentNumber
    ) {
        return ResponseEntity.status(HttpStatus.OK).body(iPersonServices.getPersonByTypeAndDocument(
                typeDocument,
                documentNumber
        ));
    }

    @Operation(summary = "Get older persons at a certain age")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Persons found",
                    content = {@Content(mediaType = "application/json",
                            array = @ArraySchema(schema = @Schema(implementation = PersonDto.class)))}),
            @ApiResponse(responseCode = "400", description = "Invalid data",
                    content = @Content),
            @ApiResponse(responseCode = "404", description = "Person not found",
                    content = @Content)})
    @GetMapping("/older/{years}")
    public ResponseEntity<List<PersonDto>> getOlderPersons(@PathVariable("years") Integer years) {
        return ResponseEntity.status(HttpStatus.OK).body(iPersonServices.getOlderPersons(years));
    }

    @PostMapping(value = "/", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    @Operation(summary = "Create a person with image", description = "The field with name person must be the json of PersonEro")
    public ResponseEntity<Long> addPerson(
            @RequestParam("image") MultipartFile image,
            @RequestParam("person") String jsonPerson
    ) {
        return ResponseEntity.status(HttpStatus.CREATED).body(iPersonServices.insert(jsonPerson, image));
    }

    @PutMapping("/")
    public ResponseEntity<Long> updatePerson(
            @RequestParam("image") MultipartFile image,
            @RequestParam("person") String jsonPerson
    ) {
        return ResponseEntity.status(HttpStatus.CREATED).body(iPersonServices.update(jsonPerson, image));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deletePerson(@PathVariable("id") Long id) {
        return new ResponseEntity(iPersonServices.delete(id), HttpStatus.OK);
    }

}

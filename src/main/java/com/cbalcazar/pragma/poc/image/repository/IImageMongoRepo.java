package com.cbalcazar.pragma.poc.image.repository;

import com.cbalcazar.pragma.poc.persistence.modelMongo.Image;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface IImageMongoRepo extends MongoRepository<Image, Long> {
}

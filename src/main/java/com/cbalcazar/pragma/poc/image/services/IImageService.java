package com.cbalcazar.pragma.poc.image.services;

import com.cbalcazar.pragma.poc.person.dto.ImageDto;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IImageService {

    List<ImageDto> getAll();

    String getById(Long id);

    Long save(MultipartFile file) throws IOException;

    void update(Long id, MultipartFile file) throws IOException;

    void delete(Long id);

}

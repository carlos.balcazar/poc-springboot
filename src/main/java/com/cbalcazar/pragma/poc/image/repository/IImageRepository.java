package com.cbalcazar.pragma.poc.image.repository;

import com.cbalcazar.pragma.poc.persistence.models.Image;
import org.springframework.data.repository.CrudRepository;

public interface IImageRepository extends CrudRepository<Image, Long> {
}

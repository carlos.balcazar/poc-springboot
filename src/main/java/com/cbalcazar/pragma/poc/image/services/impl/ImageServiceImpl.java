package com.cbalcazar.pragma.poc.image.services.impl;

import com.cbalcazar.pragma.poc.exeptions.BusinnessImageException;
import com.cbalcazar.pragma.poc.image.repository.IImageMongoRepo;
import com.cbalcazar.pragma.poc.image.services.IImageService;
import com.cbalcazar.pragma.poc.persistence.modelMongo.Image;
import com.cbalcazar.pragma.poc.person.dto.ImageDto;
import com.cbalcazar.pragma.poc.util.SequenceGeneratorService;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.transaction.Transactional;
import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Transactional
public class ImageServiceImpl implements IImageService {

    private static final String IMAGE_NOT_FOUND_MESSAGE = "Image with ID %d was not found";

    private final ModelMapper modelMapper;

    private final IImageMongoRepo iImageMongoRepo;

    private final SequenceGeneratorService sequenceGeneratorService;

    @Autowired
    public ImageServiceImpl(
            ModelMapper modelMapper,
            IImageMongoRepo iImageMongoRepo,
            SequenceGeneratorService sequenceGeneratorService
    ) {
        this.modelMapper = modelMapper;
        this.iImageMongoRepo = iImageMongoRepo;
        this.sequenceGeneratorService = sequenceGeneratorService;
    }

    @Override
    public List<ImageDto> getAll() {

        List<Image> images = iImageMongoRepo.findAll();

        return images.stream()
                .map(this::convertToDto)
                .collect(Collectors.toList());
    }

    @Override
    public String getById(Long id) {
        Image image = iImageMongoRepo.findById(id).orElseThrow(() -> new BusinnessImageException(String.format(IMAGE_NOT_FOUND_MESSAGE, id)));
        return Base64.getEncoder().encodeToString(image.getImage().getData());
    }

    @Override
    public Long save(MultipartFile file) throws IOException {

        Image image = Image.builder()
                .id(sequenceGeneratorService.generateSequence(Image.SEQUENCE_NAME))
                .image(new Binary(BsonBinarySubType.BINARY, file.getBytes()))
                .build();

        iImageMongoRepo.save(image);

        return image.getId();
    }

    @Override
    public void update(Long id, MultipartFile file) throws IOException {

        iImageMongoRepo.findById(id)
                .orElseThrow(() -> new BusinnessImageException(String.format(IMAGE_NOT_FOUND_MESSAGE, id)));

        Image image = Image.builder()
                .id(id)
                .image(new Binary(BsonBinarySubType.BINARY, file.getBytes()))
                .build();

        iImageMongoRepo.save(image);
    }

    @Override
    public void delete(Long id) {
        iImageMongoRepo.findById(id)
                .orElseThrow(() -> new BusinnessImageException(String.format(IMAGE_NOT_FOUND_MESSAGE, id)));

        iImageMongoRepo.deleteById(id);
    }

    private ImageDto convertToDto(Image image) {
        return modelMapper.map(image, ImageDto.class);
    }

}

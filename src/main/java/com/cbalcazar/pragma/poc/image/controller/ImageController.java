package com.cbalcazar.pragma.poc.image.controller;

import com.cbalcazar.pragma.poc.image.services.IImageService;
import com.cbalcazar.pragma.poc.person.dto.ImageDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/api/image")
public class ImageController {

    private final IImageService iImageService;

    @Autowired
    public ImageController(
            IImageService iImageService
    ) {
        this.iImageService = iImageService;
    }

    @GetMapping("/")
    public ResponseEntity<List<ImageDto>> allImages() {
        return new ResponseEntity(iImageService.getAll(), HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<String> oneImage(@PathVariable("id") Long id) {
        return new ResponseEntity(iImageService.getById(id), HttpStatus.OK);
    }

    @PostMapping("/")
    public ResponseEntity<Map> addImage(@RequestParam("image") MultipartFile image) {
        Map<String, Object> response = new HashMap<>();
        try {
            response.put("image_id", iImageService.save(image));
            response.put("message", "Added Successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity(response, HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity updateImage(
            @PathVariable("id") Long id,
            @RequestParam("image") MultipartFile image
    ) {
        try {
            iImageService.update(id, image);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new ResponseEntity("Update Successfully", HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteImage(@PathVariable("id") Long id) {
        iImageService.delete(id);
        return new ResponseEntity("Delete Successfully", HttpStatus.OK);
    }
}

package com.cbalcazar.pragma.poc.persistence.modelMongo;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.bson.types.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Document(collection = "Images")
public class Image {

    @Transient
    public static final String SEQUENCE_NAME = "image_id";

    @Id
    private Long id;
    private Binary image;
    private String description;
}

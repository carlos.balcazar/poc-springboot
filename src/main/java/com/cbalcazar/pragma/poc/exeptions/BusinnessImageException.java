package com.cbalcazar.pragma.poc.exeptions;

public class BusinnessImageException extends RuntimeException {

    public BusinnessImageException(String message) {
        super(message);
    }

    public BusinnessImageException(String message, Throwable cause) {
        super(message, cause);
    }

    public BusinnessImageException(Throwable cause) {
        super(cause);
    }
}

package com.cbalcazar.pragma.poc.exeptions;

public class BusinnessPersonException extends RuntimeException {

    public BusinnessPersonException() {
        super();
    }

    public BusinnessPersonException(String message) {
        super(message);
    }

    public BusinnessPersonException(String message, Throwable cause) {
        super(message, cause);
    }
}

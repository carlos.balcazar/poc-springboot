package com.cbalcazar.pragma.poc.util;

import com.cbalcazar.pragma.poc.persistence.modelMongo.Counters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.FindAndModifyOptions;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class SequenceGeneratorService {

    @Autowired
    private MongoOperations mongoOperations;

    public long generateSequence(String seqName) {
        // get the sequence number
        final Query q = new Query(Criteria.where("id").is(seqName));
        // increment the sequence number by 1
        // "seq" should match the attribute value specified in DbSequence.java class.
        final Update u = new Update().inc("seq", 1);
        // modify in document
        final Counters counter = mongoOperations.findAndModify(q, u,
                FindAndModifyOptions.options().returnNew(true).upsert(true), Counters.class);

        return !Objects.isNull(counter) ? counter.getSeq() : 1;
    }

}

package com.cbalcazar.pragma.poc.util;

import com.cbalcazar.pragma.poc.persistence.models.City;
import com.cbalcazar.pragma.poc.persistence.models.Person;
import com.cbalcazar.pragma.poc.persistence.models.TypesDocument;
import com.cbalcazar.pragma.poc.person.dto.CityDto;
import com.cbalcazar.pragma.poc.person.dto.PersonDto;
import com.cbalcazar.pragma.poc.person.dto.TypesDocumentDto;

import java.time.LocalDate;


public class DataPersonTest {

    public static final PersonDto PERSON_DTO = PersonDto
            .builder()
            .id(1L)
            .firstName("Carlos")
            .lastName("Balcazar")
            .typesDocuments(TypesDocumentDto.builder().id(1).build())
            .documentNumber("1151956458")
            .birthDate(LocalDate.parse("1994-12-08"))
            .cities(CityDto.builder().id(1).build())
            .imageId(1L)
            .build();

    public static final PersonDto PERSON_DTO2 = PersonDto
            .builder()
            .id(2L)
            .firstName("Jimena")
            .lastName("Camayo")
            .typesDocuments(TypesDocumentDto.builder().id(2).build())
            .documentNumber("123456789")
            .birthDate(LocalDate.parse("2010-05-02"))
            .cities(CityDto.builder().id(2).build())
            .imageId(2L)
            .build();

    public static final PersonDto PERSON_DTO3 = PersonDto
            .builder()
            .id(3L)
            .firstName("Jaime")
            .lastName("Rojas")
            .typesDocuments(TypesDocumentDto.builder().id(1).build())
            .documentNumber("115981715")
            .birthDate(LocalDate.parse("1999-01-25"))
            .cities(CityDto.builder().id(3).build())
            .imageId(3L)
            .build();

    public static  PersonDto createPersonDto1(){
        return PersonDto
                .builder()
                .id(1L)
                .firstName("Carlos")
                .lastName("Balcazar")
                .typesDocuments(TypesDocumentDto.builder().id(1).build())
                .documentNumber("1151956458")
                .birthDate(LocalDate.parse("1994-12-08"))
                .cities(CityDto.builder().id(1).build())
                .imageId(1L)
                .build();
    }


    public static  Person createPerson1(){
        return Person
                .builder()
                .id(4L)
                .firstName("Carlos")
                .lastName("Balcazar")
                .typesDocuments(TypesDocument.builder().id(1L).build())
                .documentNumber("1151956458")
                .birthDate(LocalDate.parse("1994-12-08"))
                .cities(City.builder().id(1L).build())
                .imageId(1L)
                .build();
    }

    public static final Person PERSON = Person
            .builder()
            .id(4L)
            .firstName("Carlos")
            .lastName("Balcazar")
            .typesDocuments(TypesDocument.builder().id(1L).build())
            .documentNumber("1151956458")
            .birthDate(LocalDate.parse("1994-12-08"))
            .cities(City.builder().id(1L).build())
            .imageId(1L)
            .build();

    public static final Person PERSON2 = Person
            .builder()
            .id(2L)
            .firstName("Jimena")
            .lastName("Camayo")
            .typesDocuments(TypesDocument.builder().id(2L).build())
            .documentNumber("123456789")
            .birthDate(LocalDate.parse("2010-05-02"))
            .cities(City.builder().id(2L).build())
            .imageId(2L)
            .build();

    public static final Person PERSON3 = Person
            .builder()
            .id(3L)
            .firstName("Jaime")
            .lastName("Rojas")
            .typesDocuments(TypesDocument.builder().id(1L).build())
            .documentNumber("115981715")
            .birthDate(LocalDate.parse("1999-01-25"))
            .cities(City.builder().id(3L).build())
            .imageId(3L)
            .build();

}

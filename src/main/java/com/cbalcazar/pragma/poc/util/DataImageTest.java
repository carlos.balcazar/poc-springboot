package com.cbalcazar.pragma.poc.util;

import com.cbalcazar.pragma.poc.persistence.modelMongo.Image;
import com.cbalcazar.pragma.poc.person.dto.ImageDto;
import org.bson.BsonBinarySubType;
import org.bson.types.Binary;


public class DataImageTest {

    public static final ImageDto IMAGE_DTO = ImageDto
            .builder()
            .id(1)
            .description("imagen1")
            .build();

    public static final Image IMAGE = Image
            .builder()
            .id(1L)
            .description("imagen1")
            .image(new Binary(BsonBinarySubType.BINARY, "imagen1".getBytes()))
            .build();

    public static final Image IMAGE2 = Image
            .builder()
            .id(2L)
            .description("imagen2")
            .image(new Binary(BsonBinarySubType.BINARY, "imagen2".getBytes()))
            .build();

    public static final Image IMAGE3 = Image
            .builder()
            .id(3L)
            .description("imagen3")
            .image(new Binary(BsonBinarySubType.BINARY, "imagen3".getBytes()))
            .build();

}
